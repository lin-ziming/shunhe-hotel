import { http } from '@kit.NetworkKit'

interface Option {
  url: string
  method?: http.RequestMethod
  data?: {
    [propName: string]: any
  }
  header?: {
    [propName: string]: any
  }
  connectTimeout?: number
  readTimeout?: number
}

export default function httpRequest(option: Option): Promise<any> {
  // 请求是异步的
  return new Promise((resolve, reject) => {
    // 演示时 http 地址需要改变 标记△△△
    let baseUrl = 'http://192.168.31.61:3000'
    let httpRequest = http.createHttp()
    httpRequest.request(
      baseUrl + option.url,
      {
        method: option.method || http.RequestMethod.GET,
        // 开发者根据自身业务需要添加header字段
        header: {
          //  默认是   'Content-Type' ,JSON格式
          'Content-Type': 'application/json',
          ...option.header
        },
        // 当使用POST请求时此字段用于传递内容
        extraData: option.data,
        expectDataType: http.HttpDataType.STRING, // 可选，指定返回数据的类型
        usingCache: true, // 可选，默认为true
        priority: 1, // 可选，默认为1
        connectTimeout: 60000, // 可选，默认为60000ms
        readTimeout: 60000, // 可选，默认为60000ms
        usingProtocol: http.HttpProtocol.HTTP1_1, // 可选，协议类型默认值由系统自动指定
      },
      (err, data) => {
        // 当该请求使用完毕时，调用destroy方法主动销毁
        httpRequest.destroy()
        if (!err) {
          resolve(JSON.parse(data.result as string))
        } else {
          reject(err)
        }
      }
    )
  })
}