export class CollectUrlEvent {
  /**
   * 是否操作成功
   * 用来表示这次操作（如收藏链接）是否成功完成
   */
  isSuccess: boolean

  /**
   * 链接地址
   * 表示被收藏或操作的网页链接
   */
  link: string

  /**
   * 错误信息
   * 如果操作失败，这里将包含具体的错误信息
   */
  errMsg: string

  /**
   * 描述信息（可选）
   * 额外的描述信息，可能用于显示给用户或用于日志记录
   */
  desc?: string | null

  /**
   * 图标链接（可选）
   * 可能与链接相关的图标或图片的URL
   */
  icon?: string | null

  /**
   * 唯一标识符（可选）
   * 可能是收藏链接的唯一ID，用于数据库中的标识
   */
  id?: number | null

  /**
   * 名称（可选）
   * 可能是链接的名称或标题，用于显示给用户
   */
  name?: string | null

  /**
   * 排序顺序（可选）
   * 用于决定在列表中的显示顺序
   */
  order?: number | null

  /**
   * 用户ID（可选）
   * 与这次操作关联的用户ID，用于标识是哪个用户进行的操作
   */
  userId?: number | null

  /**
   * 是否可见（可选）
   * 用来表示这个链接是否应该被显示给用户，可能是一个标志位
   */
  visible?: number | null

  constructor(
    isSuccess: boolean,
    link: string,
    errMsg: string,
    desc?: string,
    icon?: string,
    id?: number,
    name?: string,
    order?: number,
    userId?: number,
    visible?: number,
  ) {
    this.isSuccess = isSuccess
    this.link = link
    this.desc = desc
    this.icon = icon
    this.id = id
    this.name = name
    this.order = order
    this.userId = userId
    this.visible = visible
    this.errMsg = errMsg
  }
}


export class ThemeChangeEvent {
  themeNum: number

  constructor(themeNum: number) {
    this.themeNum = themeNum
  }
}


/**
 * 头像更换事件
 */
export class AvatarChangeEvent {
  avatarNumber: number

  constructor(avatarNumber: number) {
    this.avatarNumber = avatarNumber
  }
}


/**
 * 取消收藏事件
 */
export class UncollectEvent {
  isSuccess: boolean
  id: number
  errMsg?: string

  constructor(
    isSuccess: boolean,
    id: number,
    errMsg?: string) {
    this.isSuccess = isSuccess
    this.id = id
    this.errMsg = errMsg
  }
}

/**
 * 收藏事件
 */
export class CollectEvent {
  isSuccess: boolean
  id: number
  errMsg?: string

  constructor(
    isSuccess: boolean,
    id: number,
    errMsg?: string) {
    this.isSuccess = isSuccess
    this.id = id
    this.errMsg = errMsg
  }
}


/**
 * 用户信息更新
 */
export class UserInfoUpdateEvent {
  isUpdating: boolean

  constructor(isUpdating: boolean) {
    this.isUpdating = isUpdating
  }
}

/**
 * 登出 event
 */
export class LoginOutEvent {
}

/**
 * 登录、注册 event
 */
export class LoginRegistEvent {
  isSuccess: boolean
  errMsg?: string

  constructor(
    isSuccess: boolean,
    errMsg?: string) {
    this.isSuccess = isSuccess
    this.errMsg = errMsg
  }
}